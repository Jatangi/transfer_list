import React from 'react'

export const Card = (props) => {
  const {data}=props
  return (
  
    <div className="text-2xl flex flex-col border-2  pt-10 pb-20 pr-60 pl-5 w-[400px] h-[80vh] space-y-5 ">
      {data.map((each) =>
        <div>
          <input type="checkbox" value={each} />
          <label >{each}</label>
        </div>
      )}

    </div>
  )
}
