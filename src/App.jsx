import React from 'react'
import { useState } from 'react'
import data from './countries_1.json'
import { Card } from './components/card'
import './index.css'



export const App = () => {

  const Lefttoright = () => {
    let newList = [...list2]
    let newList2 = []
    let alldivs = document.getElementById('left-items')
    let allinputs = alldivs.getElementsByTagName('input')
    //console.log(allinputs.document.getElementsByTagName('lable'))
    for (let index = 0; index < allinputs.length; index++) {
      if (allinputs[index].checked) {
        newList = [...newList, allinputs[index].value]
      }
      else {
        newList2 = [...newList2, allinputs[index].value]
      }
    }
    setList2(newList)
    setList1(newList2)
  }
  const Righttoleft = () => {
    let newlist1 = [...list1]
    let newlist2 = []
    let alldivs = document.getElementById('right-items')
    let allinputs = alldivs.getElementsByTagName('input')
    for (let index = 0; index < allinputs.length; index++) {
      if (allinputs[index].checked) {
        newlist1 = [...newlist1, allinputs[index].value]
      }
      else {
        newlist2 = [...newlist2, allinputs[index].value]
      }

    }
    setList1(newlist1)
    setList2(newlist2)
  }
  const leftall = () => {
    let newlist2 = [...list2]
    let newlist1 = []
    let alldivs = document.getElementById('left-items')
    let allinputs = alldivs.getElementsByTagName('input')
    //console.log(allinputs.document.getElementsByTagName('lable'))
    for (let index = 0; index < allinputs.length; index++) {
      
        newlist2 = [...newlist2, allinputs[index].value]
    }
    setList2(newlist2)
    setList1(newlist1)
    }

    const rightall = () => {
      let newlist1 = [...list1]
      let newlist2 = []
      let alldivs = document.getElementById('right-items')
      let allinputs = alldivs.getElementsByTagName('input')
      //console.log(allinputs.document.getElementsByTagName('lable'))
      for (let index = 0; index < allinputs.length; index++) {
        
          newlist1 = [...newlist1, allinputs[index].value]
      }
      setList2(newlist2)
      setList1(newlist1)
      }
   
  


  let initialList1 = ["JS", "HTML", "CSS", "TS"]
  let initialList2 = ["React", "Angular", "Vue", "sevlet"]

  let [list1, setList1] = useState(initialList1)
  let [list2, setList2] = useState(initialList2)

  return (


    <>
    <header className="border-2 p-10 text-center text-3xl">Transfer List</header>
      <div className="flex flex-row justify-center mt-14">
        <div id="left-items">
          <Card data={list1} />
        </div>
        <div className="text-1xl  flex flex-col border-2 pl-5 pr-5 pt-10 space-y-5 ">
          <button onClick={rightall}>{`<<`}</button>
          <button onClick={Righttoleft}>{`<`}</button>
          <button onClick={Lefttoright}>{`>`}</button>
          <button onClick={leftall}>{`>>`}</button>
        </div>
        <div id="right-items">
          <Card data={list2} />
        </div>

      </div>
    </>


  )
}

